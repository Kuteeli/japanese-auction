// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./LotRoom.sol";

contract Auction is ERC721 {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    address[] lots;

    event LotCreated(address newLot);

    constructor() ERC721("Japanese Auction", "Lots"){}

    function createLot( uint _ETH_step, uint _price) public payable returns(address){
        _tokenIds.increment();
        uint256 newTokenId = _tokenIds.current();
        ERC721Handler handler = new ERC721Handler(address(this), newTokenId);
        LotRoom lot = new LotRoom(handler, newTokenId, _ETH_step, _price, msg.sender);
        address lotsAddress = address(lot);
        lots.push(lotsAddress);
        _mint(address(handler), newTokenId);
        emit LotCreated(address(lotsAddress));
        return lotsAddress;
    }

    function getLots(address _sender) public view returns(address[] memory){
        uint256 count = 0;
        for(uint i=0; i<lots.length; i++){
            LotRoom lot = LotRoom(payable(lots[i]));
            if(lot.isMember(_sender) || lot.isWaiting()){
                count++;
            }
        }
        address[] memory result = new address[](count);
        uint index = 0;
        for(uint i=0; i<lots.length; i++){
            LotRoom lot = LotRoom(payable(lots[i]));
            if(lot.isMember(_sender) || lot.isWaiting()){
                result[index]=lots[i];
                index++;
            }
        }
        return result;
    }
}

