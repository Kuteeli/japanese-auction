// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;
import "./ERC721Handler.sol";

contract LotRoom {
    uint256 tokenID;

    uint256 initBlock;
    uint256 finalBlock;
    uint256 block_step = 4;
    uint256 waiting_status = 8;

    uint256 ETH_step;
    uint256 price;
    uint256 deposit = 0.0001 ether;

    mapping(address => uint) room;
    uint mapSize = 0;

    address public owner;

    event RoomUpdated(uint roomLength);
    ERC721Handler nftHandler;
    event NFTOwnerUpdated(address _owner);

    constructor(
        ERC721Handler _nftHandler,
        uint256 _tokenID,
        uint256 _ETH_step,
        uint256 _price,
        address _owner
    ) {
        tokenID = _tokenID;

        ETH_step = _ETH_step * (1 wei);
        price = _price * (1 wei);
        initBlock = block.number;

        owner = _owner;
        nftHandler = _nftHandler;
    }

    function doesExist(address addr) public view returns(bool){
        return room[addr] > 0;
    }

    function push(address addr, uint dep) public{
        require(room[addr] == 0, "The address is already exists in the room");
        room[addr] = dep;
        mapSize++;
    }

    function remove(address addr) public{
        require(room[addr] != 0, "The address doesn't exist in the room");
        delete room[addr];
        mapSize--;
    }

    function getDepositFromAddress(address addr) public view returns(uint){
        return room[addr];
    }

    function getCountOfMembers() public view returns(uint){
        return mapSize;
    }







    receive() external payable {}

    function onlyWaiting() private view {
        require(
            block.number <= initBlock + waiting_status,
            "The room is not waiting any more"
        );
    }

    function onlyStarted() private view {
        require(
            block.number > initBlock + waiting_status,
            "The room has not started"
        );
    }

    function onlyOneInTheRoom() private view {
        require(
            this.getCountOfMembers() == 1,
            "Members != 1"
        );
    }

    function raiseHand() public payable {
        onlyWaiting();
        require(msg.value >= deposit, "Incorrect ether amount");
        this.push(msg.sender, msg.value);
        emit RoomUpdated(this.getCountOfMembers());
    }

    function quit() public payable {
        onlyStarted();
        payable(msg.sender).transfer(this.getDepositFromAddress(msg.sender));
        this.remove(msg.sender);
        if (this.getCountOfMembers() == 1) {
            finalBlock = block.number - 1;
        }
        emit RoomUpdated(this.getCountOfMembers());
    }

    function buy() public payable {
        onlyOneInTheRoom();
        require(
            this.getCountOfMembers() == 1 && this.getDepositFromAddress(msg.sender) > 0,
            "Caller is not the winner"
        );
        require(!nftHandler.isOwner(msg.sender), "Already owned");
        uint256 finalPrice = getFinalPrice();
        uint256 _deposit = this.getDepositFromAddress(msg.sender);
        if (finalPrice > _deposit) {
            require(
                msg.value == (finalPrice - _deposit),
                "not enough money"
            );
            payable(owner).transfer(_deposit);
            payable(owner).transfer(msg.value);
        } else if (finalPrice == _deposit) {
            payable(owner).transfer(_deposit);
        } else {
            payable(owner).transfer(finalPrice);
            payable(msg.sender).transfer(_deposit - finalPrice);
        }
        nftHandler.transfer(msg.sender);
        emit NFTOwnerUpdated(msg.sender);
    }

    function getFinalPrice() public view returns (uint256) {
        if (getCountOfMembers() == 0
        || (finalBlock == 0 && getCountOfMembers() == 1)
            || block.number < initBlock + waiting_status
        ) {
            return price;
        }
        if(finalBlock > 0){
            return price + ((finalBlock - initBlock - waiting_status) / block_step) * ETH_step;
        }
        return price + ((block.number - initBlock - waiting_status) / block_step) * ETH_step;
    }

    function getInfo()
    public
    view
    returns (
        uint256,
        uint256,
        uint256,
        uint256,
        uint256,
        uint256,
        uint256,
        uint256
    )
    {
        return (
        tokenID,
        initBlock,
        getFinalPrice(),
        deposit,
        ETH_step,
        waiting_status,
        block_step,
        getCountOfMembers()
        );
    }

    function isMember(address _sender)
    public view returns(bool){
        return this.getDepositFromAddress(_sender) > 0;
    }

    function isNFTOwner(address _sender)
    public view returns(bool){
        return nftHandler.isOwner(_sender);
    }
    function isWaiting()
    public view returns(bool){
        return block.number < initBlock + waiting_status;
    }
}
