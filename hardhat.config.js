/**
 * @type import('hardhat/config').HardhatUserConfig
 */
require('dotenv').config();
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-etherscan");

const { PRIVATE_KEY, REACT_APP_INFURA_API_KEY, ETHERSCAN_API_KEY} = process.env;
module.exports = {
  solidity: "0.8.17",
  defaultNetwork: "goerli",
  networks: {
    hardhat: {},
    ganache: {
      url: `HTTP://127.0.0.1:7545`,
      accounts: [`0x${PRIVATE_KEY}`],
      chainId: 1337
    },
    goerli: {
      url: `https://goerli.infura.io/v3/${REACT_APP_INFURA_API_KEY}`,
      accounts: [`0x${PRIVATE_KEY}`]
    }
  },
  paths: {artifacts: "./src/artifacts"},
  etherscan: {
    apiKey: ETHERSCAN_API_KEY
  },
}