import React from "react";
import { useEffect, useState} from "react";
import EventBus from "./services/EventBus";
import Auth from "./pages/Auth";
import Main from "./pages/Main";
import {Routes, Route, BrowserRouter as Router} from "react-router-dom";
import { ProtectedRoute } from "./components/ProtectedRoute";
import SignerContext from "./context/Signer";
import Error from "./pages/Error";
import {ethers} from "ethers";
import InfuraProviderContext from "./context/InfuraProviderContext";

function App() {
  const [signer, setSigner] = useState(undefined);

  useEffect(() => {
    const unsubscribeChangeSigner = EventBus.on('changeSigner', (signer) => { setSigner(signer) })
    return () => {
      unsubscribeChangeSigner()
    }
  }, [])
  return (
      <Router>
          <Routes>
              <Route path="/" element={
                  <SignerContext.Provider value={signer}>
                      <Auth/>
                  </SignerContext.Provider>
              }/>
              <Route path="/main" element={
                  <SignerContext.Provider value={signer}>
                      <ProtectedRoute>
                          <InfuraProviderContext.Provider value={
                              process.env.REACT_APP_NETWORK_VERSION == 5
                                  ? new ethers.providers.InfuraProvider("goerli", "126151b45f2245e496725f8c6b0beab0")
                                  : new ethers.providers.Web3Provider(window.ethereum)
                          }>
                            <Main/>
                          </InfuraProviderContext.Provider>
                      </ProtectedRoute>
                  </SignerContext.Provider>
              }/>
              <Route path="/error" element={
                  <SignerContext.Provider value={signer}>
                      <Error/>
                  </SignerContext.Provider>
              }/>
          </Routes>
      </Router>
  );
}

export default App;
