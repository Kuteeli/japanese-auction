import {Navigate} from "react-router-dom";
import {useContext} from "react";
import SignerContext from "../context/Signer";

export const ProtectedRoute = ({ children }) => {
    const signer = useContext(SignerContext)
    if (signer === undefined) {
        return <Navigate to="/" replace />;
    }
    return children;
};