import React, {useContext, useEffect, useState} from 'react';
import QuitButton from "../buttons/QuitButton";
import {timeDisplay} from "../../services/timeDisplay";
import members from "../../assets/members.png";
import clock from "../../assets/clock.png";
import eth from "../../assets/ethereum.png";
import NetworkBlockNumber from "../../context/NetworkBlockNumber";
import {ethers} from "ethers";
import BuyButton from "../buttons/BuyButton";
import ShowHistory from "../buttons/ShowHistory";

const AuctionInProgress = ({ LotInf, contract, infuraContract, setLotInf, isNFTOwner, isMember, address}) => {

    const currentBlockNumber = useContext(NetworkBlockNumber);
    const [_time, _setTime] = useState(LotInf.blockStep);
    useEffect(() => {
        if(LotInf.members > 1){
            const leftTime = (currentBlockNumber - LotInf.initBlock - LotInf.waitingStatus) % LotInf.blockStep

            if(leftTime === 0){
                _setTime(0)
                infuraContract.getFinalPrice().then(newPrice => {
                    setLotInf(prevValue => ({...prevValue, price:newPrice}))
                    _setTime(LotInf.blockStep)
                })
            } else{
                _setTime(LotInf.blockStep - leftTime)
            }
        }
    }, [currentBlockNumber])

    return <>
        <div className="auction_in_progress_container">
            <div className="auction_in_progress_header">
                <h2 className="auction_in_progress_token_id"><strong>NFT ID: </strong>{LotInf.tokenID}</h2>
                {isNFTOwner && <h2><strong>Owned</strong></h2>}
            </div>
            <div className="auction_in_progress_main">
                <h4 className="current_price">
                    price: {ethers.utils.formatEther(LotInf.price)}
                    <img src={eth} alt="" className="eth"/>
                </h4>
                {
                    LotInf.members>1
                        ? <h4 className="next_price">
                            next price: {ethers.utils.formatEther(LotInf.price?.add(LotInf.ETH_step))}
                            <img src={eth} alt="" className="eth"/>
                        </h4>
                        : <></>}

            </div>
            <div className="auction_in_progress_footer">
                <div className="footer_info">
                    { LotInf.members === 1 ? <></> :   <h3 className="time_to_price_increase">
                        <img src={clock} alt="" className="clock_logo"/>
                        {timeDisplay(_time * 15)} min
                    </h3>}
                    <h3 className="members">
                        {LotInf.members}
                        <img src={members} alt="" className="members_logo"/>
                    </h3>
                </div>
                <div className="footer_btn">
                    <ShowHistory address={address}/>
                    { LotInf.members === 1 && isMember
                        ? isNFTOwner
                            ? <></>
                            : <BuyButton handler={() => {
                                let options = {}
                                if(LotInf.deposit < LotInf.price) {
                                    options = {value: LotInf.price.sub(LotInf.deposit).toString()}
                                }
                                contract.buy(options)
                            }}/>
                        : <QuitButton handler={() => {
                            contract.quit()
                        }}/>
                    }
                </div>
            </div>
        </div>
        <style jsx>{`
        .auction_in_progress_container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100%;
            width: 100%;
            padding: 2vh 2vw 2vh 40px;
        }
        .auction_in_progress_header {
            display: flex;
            align-items: center;
            justify-content: space-between;
            flex-grow: 1;
            width: 100%;
        }
        .auction_in_progress_token_id {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
        .auction_in_progress_main {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: flex-start;
            flex-grow: 4;
            width: 100%;
            gap: 4px;
        }
        
        .time_to_price_increase, .members, .current_price, .next_price {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px;
        }
        
        .clock_logo, .members_logo, .eth  {
            width: 40px;
            height: 40px;
        }
        
        .auction_in_progress_footer {
            display: inline-flex;
            align-items: center;
            flex-grow: 1;
            width: 100%;
            justify-content: space-between;
        }
        
        .footer_info {
            display: flex;
            justify-content: space-between;
            align-items: center;
            width: 30%;
        }
        
        .footer_btn {
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 20px;
        }
        
        h2, h3, h4 {
            margin: 0;
        }
        `}</style>
    </>
};

export default AuctionInProgress;