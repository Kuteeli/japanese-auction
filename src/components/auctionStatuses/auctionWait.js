import React, {useContext, useEffect, useMemo, useState} from 'react';
import {timeDisplay} from "../../services/timeDisplay";
import RaiseHandButton from "../buttons/RaiseHandButton";
import eth from "../../assets/ethereum.png"
import member from "../../assets/iAmMember.png"
import rocket from "../../assets/rocket.gif"
import {ethers} from "ethers";
import NetworkBlockNumber from "../../context/NetworkBlockNumber";
import ShowHistory from "../buttons/ShowHistory";

const AuctionWait = ({ LotInf, contract, isMember, address }) => {

    const currentBlockNumber = useContext(NetworkBlockNumber);
    const checkBlock = useMemo(() => {
        return ((LotInf.initBlock + LotInf.waitingStatus) - currentBlockNumber) * 15
    }, [LotInf, currentBlockNumber])
    const [waitingTime, setWaitingTime] = useState(checkBlock)
    const [isLoading, setIsLoading] = useState(false)
    useEffect(() => {
        setWaitingTime(checkBlock)
    }, [LotInf, currentBlockNumber])

    useEffect(() => {
        window.ethereum.on('accountsChanged', function () {
            setIsLoading(false)
        })
    }, [])

    useEffect(() => {
        if (isMember){ setIsLoading(false) }
    }, [isMember])

    return <>
        <div className="auction_wait_container">
            <div className="auction_wait_header">
                <h3 className="auction_wait_token_id"><strong>NFT ID: </strong>{LotInf.tokenID}</h3>
                <h3 className="auction_wait_price">
                    <strong>{ethers.utils.formatEther(LotInf.price)}</strong>
                    <img src={eth} alt="" className="auction_wait_logo"/>
                </h3>
            </div>
            <div className="auction_wait_footer">
                <h3 className="auction_wait_time">Waiting time: <strong>{timeDisplay(waitingTime)}</strong> min</h3>
                <div className="auction_wait_btns">
                    <ShowHistory address={address}/>
                    { isMember && !isLoading ? <h3 className="member">You are a member<img src={member} alt="" className="auction_wait_logo"/></h3> : <></>}
                    { !isMember && !isLoading ? <RaiseHandButton deposit={LotInf.deposit} handler={() => {
                        setIsLoading(true)
                        const options = {value: LotInf.deposit.toString()}
                        contract.raiseHand(options).catch(() => {
                            setIsLoading(false)
                        })
                    }}/> : <></>}
                    { isLoading ? <h3 className="loading">Loading<img src={rocket} alt="" className="loading_img" /></h3> : <></> }
                </div>
            </div>
        </div>
        <style jsx>{`
            .auction_wait_container{
                width: 100%;
                height: 100%;
                display: flex;
                flex-direction: column;
                padding: 4vh 2vw 4vh 2vw;
            }
            .auction_wait_header {
                display: flex;
                align-items: start;
                justify-content: space-between;
                height: 70%;
            }
            .auction_wait_footer {
                height: 30%;
                display: flex;
                align-items: center;
                justify-content: space-between;
            }
            
            .auction_wait_token_id {
                margin: 0;
                max-width: 60%;
                overflow: hidden;
                text-overflow: ellipsis;
                word-wrap: break-word;
                display: -webkit-box;
                -webkit-line-clamp: 3;
                -webkit-box-orient: vertical;
            }
            
            .auction_wait_logo {
                width: 40px;
                height: 40px;
            }
            
            .loading_img {
                width: 60px;
                height: 60px;
            }
            
            .loading {
                display: flex;
                align-items: center;
                justify-content: center;
                gap: 10px;
            }
            
            .auction_wait_price{
                display: flex;
                align-items: center;
                justify-content: center;
                gap: 0.1vw;
                margin: 0;
            }
            
            .auction_wait_starting_price {
                margin: 0;
                align-self: center;
            }
            
            .member {
                color: #0275d8;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            
            .auction_wait_btns {
                display: flex;
                justify-content: center;
                align-items: center;
                gap: 20px;
            }
            
            h3 {
              margin: 0;
            }
        `}</style>
    </>
};

export default AuctionWait;