import React from 'react'

const BuyButton = ({handler}) => {
    return <>
        <button className="btn btn-success buy-button" onClick={handler}>
            <span className={"h5"}>Buy</span>
        </button>
        <style jsx>{`
          .buy-button {
            border: none;
            color: white;
            height: 55px;
            border-radius: 4px;
            cursor: pointer;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 2px;
            padding: 0 50px;
          }
          
          .eth_money {
            width: 25px;
            height: 25px;
          }
          
          .h5 {
            margin: 0;
          }
        `}</style>
    </>;
};

export default BuyButton;