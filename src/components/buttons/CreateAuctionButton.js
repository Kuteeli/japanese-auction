import React, {useEffect, useState} from 'react';
import eventBus from "../../services/EventBus";
import {ethers} from "ethers";
import loading from "../../assets/primary_loading.gif";

const CreateAuctionButton = () => {
    const clickHandler = () => {
        eventBus.emit("Auction:createLot", {
            etherStep: ethers.utils.parseEther("0.0001"),
            price: ethers.utils.parseEther("0.0001"),
        })
    }

    const [isLoading, setIsLoading] = useState(false)
    const [isDisabled, setIsDisabled] = useState(false)
    
    useEffect(() => {
        const removeAuctionCreateLoading = eventBus.on("Auction: createAuctionLoading", (value) => {
            setIsLoading(value)
        })
        const removeAuctionCreateDisabled = eventBus.on("Auction: createAuctionDisabled", (value) => {
            setIsDisabled(value)
        })
        return () => {
            removeAuctionCreateLoading()
            removeAuctionCreateDisabled()
        }
    }, [])

    return <>
        <button className={"btn btn-success create_auction_button" + (isLoading ? " opacity" : "")} onClick={clickHandler} disabled={isDisabled}>
            { isLoading ? <img className="primary_loading" src={loading} alt=""/> : <span className={"h2"}>Create auction</span>}
        </button>
        <style jsx>{`
          .create_auction_button {
            width: 280px;
            height: 60px;
            min-width: 250px;
            display: flex;
            justify-content: center;
            align-items: center;
          }
          
          .opacity[disabled] {
            opacity: 1;
          }
          
          .primary_loading {
              width: 40px;
              height: 40px;
          }
          
          .h2 {
              margin: 0;
          }
          
        `}</style>
    </>;
};

export default CreateAuctionButton;