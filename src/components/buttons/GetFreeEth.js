import React from 'react';

const GetFreeEth = () => {
    return <>
        <button className="btn btn-primary get_eth_button" onClick={() => window.open("https://goerlifaucet.com/")}>
            <span className={"h2"}>Get ETH</span>
        </button>
        <style jsx>{`
          .get_eth_button {
            width: 280px;
            height: 60px;
            min-width: 250px;
            display: flex;
            justify-content: center;
            align-items: center;
          }
          
          .h2 {
            margin: 0;
          }
        `}</style>
    </>
};

export default GetFreeEth;