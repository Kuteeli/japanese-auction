import React from 'react';
import { loginButtonHandler } from "../../services/LoginButtonHandler";
import logo from "../../assets/metamask.svg";

const LoginButton = () => {

    return <>
        <button onClick={loginButtonHandler}>
            <span className={"h2"}>Log In</span>
            <img src={logo} alt=""/>
        </button>
        <style jsx>{`
        button {
          border: none;
          background: black;
          color: white;
          width: 300px;
          height: 70px;
          border-radius: 10px;
          cursor: pointer;
          display: flex;
          align-items: center;
          justify-content: center;
          gap: 30px;
        }
        
        button:hover{
          background: white;
          border: 1px solid black;
          color: black;
          transition: 0.3s;
        }
        
        img {
          width: 40px;
          height: 40px;
        }
        
        .h2 {
          margin: 0;
        }
      `}
        </style>
    </>
};

export default LoginButton;