import React from 'react'

const QuitButton = ({handler}) => {
    return <>
        <button className="btn btn-danger raise_hand_button" onClick={handler}>
            <span className={"h5"}>QUIT</span>
        </button>
        <style jsx>{`
          .raise_hand_button {
            border: none;
            color: white;
            height: 55px;
            border-radius: 4px;
            cursor: pointer;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 2px;
            min-width: 170px;
          }
          
          .eth_money {
            width: 25px;
            height: 25px;
          }
          
          .h5 {
            margin: 0;
          }
        `}</style>
    </>;
};

export default QuitButton;