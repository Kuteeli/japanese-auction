import React from 'react'
import ethMoney from "../../assets/eth-money.svg";
import {ethers} from "ethers";

const RaiseHandButton = ({ deposit, handler }) => {
    return <>
        <button className="btn btn-success raise_hand_button" onClick={handler}>
            <span className={"h5"}>RAISE HAND FOR {ethers.utils.formatEther(deposit)}</span>
            <img className='eth_money' src={ethMoney} alt=""/>
        </button>
        <style jsx>{`
          .raise_hand_button {
            border: none;
            color: white;
            height: 55px;
            border-radius: 4px;
            cursor: pointer;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 2px;
          }
          
          .eth_money {
            width: 25px;
            height: 25px;
          }
          
          .h5 {
            margin: 0;
          }
        `}</style>
    </>;
};

export default RaiseHandButton;