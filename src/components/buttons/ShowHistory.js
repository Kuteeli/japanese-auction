import React from 'react';

const ShowHistory = ({ address, type }) => {
    if (type === "left-side"){
        return <>
            <button className="btn btn-info show_history_button left-side" onClick={() => window.open(`https://goerli.etherscan.io/address/${address}`)}>
                <span className={"h3"}>History</span>
            </button>
            <style jsx>{`
          .show_history_button {
            border: none;
            color: white;
            height: 55px;
            border-radius: 4px;
            cursor: pointer;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 2px;
          }
          .left-side {
            min-width: 280px;
          }
          .h3 {
            margin: 0;
          }
        `}</style>
        </>
    } else {
        return <>
            <button className="btn btn-info show_history_button lot" onClick={() => window.open(`https://goerli.etherscan.io/address/${address}`)}>
                <span className={"h5"}>History</span>
            </button>
            <style jsx>{`
          .show_history_button {
            border: none;
            color: white;
            height: 55px;
            border-radius: 4px;
            cursor: pointer;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 2px;
          }
          
          .lot {
            min-width: 170px;
          }
          
          .h5 {
            margin: 0;
          }
        `}</style>
        </>
    }
};

export default ShowHistory;