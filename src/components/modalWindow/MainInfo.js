import React, {useContext, useEffect, useState} from 'react';
import SignerContext from "../../context/Signer";

const MainInfo = () => {
    const signer = useContext(SignerContext)
    const [publicKey, setPublicKey] = useState("")

    useEffect(() => {
        signer.getAddress().then((res) => { setPublicKey(res)})
    }, [])

    return <>
        <h2 className="modal__title"><strong>Как посмотреть купленный NFT в кошельке Metamask?</strong></h2>
        <p className="modal__text">
            1) Необходимо скачать мобильное приложение Metamask <br/>
            2) Войдите в кошелек <br/>
            3) На главном экране перейдите на вкладку "Токены NFT"<br/>
            4) Выбирете "Импорт NFT"<br/>
            5) В строку адреса введите адрес контракта: {process.env.REACT_APP_AUCTION_ADDRESS}<br/>
            6) В строку ID введите идентификатор купленного лота<br/>
            7) Нажимите "Импорт"<br/>
        </p>
        <h2 className="modal__title"><strong>Как получить бесплатный эфир?</strong></h2>
        <p className="modal__text">
            1) В левой части экрана нажмите на "Get ETH", откроется сайт крана <br/>
            2) В строку адреса введите id своего кошелька: {publicKey}<br/>
            3) Нажмите на кнопку "Send Me ETH" <br/>
        </p>
        <style jsx>{`
            .modal__text {
              font-size: 1.5rem;
            }
            .modal__title {
              padding-bottom: 10px;
            }
        `}</style>
    </>
}
export default MainInfo;