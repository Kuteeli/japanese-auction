import React from 'react';

const ModalWindow = ({ active, setActive, children }) => {
    return <>
        <div className={active ? "modal active" : "modal"} onClick={() => { setActive(false) }}>
            <div className="modal__content" onClick={e => e.stopPropagation()}>
                { children }
            </div>
        </div>
        <style jsx>{`
            .modal {
              height: 100vh;
              width: 100vw;
              background-color: rgba(0,0,0,0.4);
              position: fixed;
              top: 0;
              left: 0;
              display: flex;
              align-items: center;
              justify-content: center;
              transform: scale(0);
              color: black;
            }
            .modal__content {
              padding: 50px;
              border-radius: 12px;
              background-color: white;
            }
            .modal.active {
              transform: scale(1);
            }
        `}</style>
    </>
};

export default ModalWindow;