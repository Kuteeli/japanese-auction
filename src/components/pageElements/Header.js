import React, {useState} from 'react';
import flag from "../../assets/Japan-flag.png";
import ModalWindow from "../modalWindow/ModalWindow";
import question from "../../assets/question.png"
import MainInfo from "../modalWindow/MainInfo";

export const Header = () => {
    const [modalActive, setModalActive] = useState(false)

    return <>
        <header className="p-3 bg-dark text-white">
            <div className="header_container">
                <div className="header_left">
                    <img className="flag" src={flag} alt=""/>
                    <h1>Japanese auction</h1>
                </div>
                <div className="header_right">
                    <img className="header_info_button" onClick={() => setModalActive(true)} src={question} alt=""/>
                </div>
            </div>
            <ModalWindow active={modalActive} setActive={setModalActive}>
                <MainInfo/>
            </ModalWindow>
        </header>
        <style jsx>{`
            header{
              height: 8vh;
              width: 100vw;
              min-width: 1320px;
              min-height: 84px;
            }

            .header_container {
              display: flex;
              align-items: center;
              justify-content: space-between;
              margin: 0;
              width: 100%;
              padding-right: 20px;
            }
            
            .header_info_button {
              cursor: pointer;
            }
            
            .header_left {
              display: flex;
              margin: 0;
              gap: 50px;
              height: 5vh;
              white-space: nowrap;
            }
            .flag {
              min-width: 61px;
              min-height: 61px;
              width: auto;
              height: auto;
              object-fit: cover;
            }
            h1 {
              margin: auto 0;
            }
        `}</style>
    </>
};

export default Header;