import React from 'react';
import CreateAuctionButton from "../buttons/CreateAuctionButton";
import GetFreeEth from "../buttons/GetFreeEth";
import ShowHistory from "../buttons/ShowHistory";
import Transactions from "../transactionsChat/Transactions";

export const LeftAside = () => {
    return <>
        <aside className="left_aside">
            <div className="left_aside_container_btn">
                <CreateAuctionButton/>
                <GetFreeEth/>
                <ShowHistory address={process.env.REACT_APP_AUCTION_ADDRESS} type='left-side'/>
            </div>
            <div className="transactions_history">
                <Transactions/>
            </div>
        </aside>
        <style jsx>{`
          .left_aside {
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 30vw;
            height: 92vh;
            min-width: 548px;
            padding-top: 5vh;
          }
          .left_aside_container_btn{
              padding-bottom: 10vh;
              width: 15vw;
              margin: 0 auto;
              display: flex;
              flex-direction: column;
              justify-content: center;
              align-items: center;
              min-width: 250px;
              gap: 30px;
          }
          
          .transactions_history {
              width: 100%;
              flex: 1;
          }
        `}</style>
    </>;
};

export default LeftAside;