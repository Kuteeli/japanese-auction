import React, {useContext, useEffect, useState} from 'react';
import SignerContext from "../../context/Signer";
import AuctionWait from "../auctionStatuses/auctionWait";
import AuctionInProgress from "../auctionStatuses/auctionInProgress";
import useLotContract from "../../hooks/useLotContract";
import NetworkBlockNumber from "../../context/NetworkBlockNumber";
import {ethers} from "ethers";
import LotContractSol from "../../artifacts/contracts/LotRoom.sol/LotRoom.json";
import InfuraProviderContext from "../../context/InfuraProviderContext";
import eventBus from "../../services/EventBus";

const Lot = ({ address }) => {
    const currentBlockNumber = useContext(NetworkBlockNumber);

    const [LotInf, setLotInf] = useState(undefined)

    const signer = useContext(SignerContext)
    const [contract] = useLotContract(signer, address)


    const [isNFTOwner, setIsNFTOwner] = useState(false)
    const [isMember, setIsMember] = useState(false)
    const [isWaiting, setIsWaiting] = useState(true)

    const InfuraProvider = useContext(InfuraProviderContext)
    const [publicKey, setPublicKey] = useState(undefined)
    const [infuraContract, setInfuraContract] = useState(undefined)

    useEffect(() => {
        if(InfuraProvider){
            const newContract = ethers.ContractFactory.fromSolidity(LotContractSol)
                .attach(address)
                .connect(InfuraProvider)
            setInfuraContract(newContract)
        }
    }, [InfuraProvider])
    useEffect(() => {
        signer.getAddress().then(_publicKey => {
            setPublicKey(_publicKey);
        })
    },[signer])
    useEffect(() => {
        if(infuraContract && publicKey){
            infuraContract.getInfo().then(([
                                         tokenID,
                                         initBlock,
                                         price,
                                         deposit,
                                         ETH_step,
                                         waiting_status,
                                         block_step,
                                         members
                                     ]) => {
                const newContractInfo = {
                    tokenID: tokenID.toString(),
                    price, //начальная цена лота в эфирах
                    deposit, // сумма за принятие участия
                    ETH_step, // шаг роста price
                    waitingStatus: waiting_status.toNumber(), // сколько блоков длится ожидание
                    blockStep: block_step.toNumber(), // шаг повышения цены
                    initBlock: initBlock.toNumber(), // текущий блок
                    members: members.toNumber(),
                };
                setLotInf(prevState => ({...prevState, ...newContractInfo}));
            })

            infuraContract.isMember(publicKey).then((value) => {
                setIsMember(value)
            })
            infuraContract.isNFTOwner(publicKey).then((value) => {
                setIsNFTOwner(value)
            })
            infuraContract.isWaiting().then((value) => {
                setIsWaiting(value)
            })
            const RoomUpdated = (roomLength, event) => {
                setLotInf(prevState => {
                    eventBus.emit("TransactionHistory:addEvent", {
                        token: `The room of the lot ${prevState.tokenID} updated`,
                        txHash: event.transactionHash
                    })
                    return {
                        ...prevState,
                        members: roomLength.toNumber()
                    }
                })
                infuraContract.isMember(publicKey).then((value) => {
                    setIsMember(value)
                })
            }
            infuraContract.on("RoomUpdated", RoomUpdated)

            const NFTOwnerUpdated = (newOwner, event) => {
                setLotInf(prevState => {
                    eventBus.emit("TransactionHistory:addEvent", {
                        token: `The lot ${prevState.tokenID} was bought`,
                        txHash: event.transactionHash
                    })
                    return prevState
                })
                setIsNFTOwner(newOwner)
            }
            contract.on("NFTOwnerUpdated", NFTOwnerUpdated)

            return () => {
                contract.off("RoomUpdated", RoomUpdated)
                contract.off("NFTOwnerUpdated", NFTOwnerUpdated)
            }
        }
    }, [publicKey, infuraContract])


    useEffect(() => {
        infuraContract?.isWaiting()?.then(value => {
            setIsWaiting(value)
        })
    }, [currentBlockNumber, infuraContract])

    return <>
        {LotInf && (isMember || isWaiting) && <div className="lot_container">
            {
                isWaiting
                    ? <AuctionWait
                        LotInf={LotInf}
                        contract={contract}
                        isMember={isMember}
                        address={address}
                    />
                    : <AuctionInProgress
                        LotInf={LotInf}
                        isMember={isMember}
                        setLotInf={setLotInf}
                        contract={contract}
                        infuraContract={infuraContract}
                        isNFTOwner={isNFTOwner}
                        address={address}
                    />
            }
        </div>}
        <style jsx>{`
          .lot_container {
            border: 1px solid black;
            width: 100%;
            height: 24vh;
            border-radius: 10px;
            flex: 0 0 auto;
            min-width: 900px;
          }
        `}</style>
    </>;
};

export default Lot;