import React from 'react';
import metamask from "../../assets/metamask.svg"

const MetamaskInvalid = () => {
    return <>
        <div className="metamask-invalid-container">
            <div className="metamask-invalid-text">
                <img src={metamask} alt="404"/>
                <h1>You need to install the metamask plugin</h1>
                <button className="btn btn-success" onClick={() => window.open("https://metamask.io/download/")}>Download plugin</button>
            </div>
        </div>
        <style jsx>{`
            .metamask-invalid-container {
              padding-top: 20px;
              background: white;
              position: fixed;
              left: 0px;
              top: 0;
              width: 100%;
              height: 100%;
              line-height: 1.5em;
              z-index: 9999;
            }
            .metamask-invalid-text {
              padding-top: 40px;
            }
            .metamask-invalid-container, .metamask-invalid-text {
              display: flex;
              flex-direction: column;
              gap: 20px;
              align-items: center;
              color: #000;
            }
            .metamask-invalid-text img {
              height: 150px;
            }
            .metamask-invalid-text h1 {
              font-size: 3.3em;
              font-weight: 900;
              margin: 0;
            }
        `}
        </style>
    </>
};

export default MetamaskInvalid;