import React, {useContext, useEffect, useState} from 'react';
import SignerContext from "../../context/Signer";
import eventBus from "../../services/EventBus";
import Lot from "./Lot";
import useAuctionContract from "../../hooks/useAuctionContract";
import useBlockNumber from "../../hooks/useBlockNumber";
import NetworkBlockNumber from "../../context/NetworkBlockNumber";
import InfuraProviderContext from "../../context/InfuraProviderContext";

export const RightAside = () => {
    const [LotsList, setLotsList] = useState([])
    const signer = useContext(SignerContext);
    useAuctionContract(signer);
    useEffect(() => {
        const removeLotsListUpdate = eventBus.on("Auction:UpdateAll", (newValue) => {
            setLotsList(newValue)
        })
        const removeLotCreated = eventBus.on("Auction:LotCreated", (value) => {
            setLotsList(prevState => {
                if(prevState.includes(value)) return prevState
                return [...prevState, value]
            })
        })
        return () => {
            removeLotsListUpdate()
            removeLotCreated()
        }
    }, [])
    useEffect(() => {
        eventBus.emit("Auction: createAuctionLoading", false)
        eventBus.emit("Auction: createAuctionDisabled", false)
    }, [LotsList])
    const currentBlockNumber = useBlockNumber(useContext(InfuraProviderContext));
    return <>
        <aside className="right_aside">
            <NetworkBlockNumber.Provider value={currentBlockNumber}>
                <div className="lots_list">
                    {LotsList?.map(item => <Lot key={item} address={item}/>)}
                </div>
            </NetworkBlockNumber.Provider>
        </aside>
        <style jsx>{`
          .right_aside {
            width: 70vw;
            height: 92vh;
            padding-top: 5vh;
            padding-bottom: 5vh;
          }
          .lots_list{
              width: 40vw;
              height: 100%;
              margin: 0 auto;
              display: flex;
              flex-direction: column;
              gap: 5vh;
              overflow-y: scroll;
              min-width: 920px;
          }
          
          ::-webkit-scrollbar {
              width: 0.375rem;
          }
          ::-webkit-scrollbar-track {
              background: white;
          }
          ::-webkit-scrollbar-thumb {
              background: #888;
              border-radius: 5px;
          }
          ::-webkit-scrollbar-thumb:hover {
              background: #555; 
          }
          
        `}</style>
    </>;
};

export default RightAside;