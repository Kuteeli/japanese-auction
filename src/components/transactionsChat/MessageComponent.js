import React from 'react';

const MessageComponent = ({ token, txHash}) => {
    return <>
        <div className="message_container">
            <h5 className="message_url" onClick={() => window.open(`https://goerli.etherscan.io/tx/${txHash}`)}><strong>{token}</strong></h5>
        </div>
        <style jsx>{`
            .message_container {
                display: flex;
                align-items: center;
                justify-content: center;
                gap: 10px;
                background-color: rgba(0, 120, 201, 0.2);
                padding: 20px;
                width: 70%;
                border-radius: 12px;
            }
            .message_url {
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .message_url:hover {
                cursor: pointer;
                text-decoration: underline;
            }
            h5 {
                margin: 0;
            }
       `}</style>
    </>
};

export default MessageComponent;