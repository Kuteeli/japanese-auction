import React, {useContext, useEffect, useState} from 'react';
import MessageComponent from "./MessageComponent";
import eventBus from "../../services/EventBus";
import SignerContext from "../../context/Signer";

const Transactions = () => {
    const [messages, setMessage] = useState([])
    const signer = useContext(SignerContext)
    useEffect(() => {
        setMessage([])
    }, [signer])

    useEffect(() => {
        const removeTransactionHistoryAddEvent = eventBus.on("TransactionHistory:addEvent", (payload) => {
            setMessage(value => [...value, payload])
        })
        return removeTransactionHistoryAddEvent
    }, [])

    useEffect(() => {
        if (messages.length > 7) {
            setMessage(value => value.slice(messages.length - 7))
        }
    })

    return <>
        <div className="transactions_container">
            <div className="transactions_header">
                <h1>Transactions</h1>
            </div>
            <div className="transactions_body">
                {
                    messages.map(message => <MessageComponent key={message.txHash} token={message.token} txHash={message.txHash}/>)
                }
            </div>
        </div>
        <style jsx>{`
            .transactions_container {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }
            .transactions_body {
                padding-top: 20px;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                gap: 20px;
                width: 100%;
            }
       `}</style>
    </>
};

export default Transactions;