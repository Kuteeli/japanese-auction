import {useContext, useEffect, useState} from "react";
import {ethers, Signer} from "ethers";
import AuctionContractSol from "../artifacts/contracts/Auction.sol/Auction.json";
import eventBus from "../services/EventBus";
import InfuraProviderContext from "../context/InfuraProviderContext";

const useAuctionContract = (signer)=> {
    const [AuctionContract, setAuctionContract] = useState(undefined);
    const InfuraProvider = useContext(InfuraProviderContext)

    useEffect(() =>{
        if(Signer.isSigner(signer)){
            const Auction = ethers.ContractFactory.fromSolidity(AuctionContractSol)
                .attach(process.env.REACT_APP_AUCTION_ADDRESS)
                .connect(signer);
            setAuctionContract(Auction);
            const removeCreateLot = eventBus.on("Auction:createLot", ({etherStep, price}) => {
                eventBus.emit("Auction: createAuctionDisabled", true)
                Auction.createLot(etherStep, price).then(() => {
                    eventBus.emit("Auction: createAuctionLoading", true)
                }).catch(() => {
                    eventBus.emit("Auction: createAuctionLoading", false)
                    eventBus.emit("Auction: createAuctionDisabled", false)
                });
            });
            return () => {
                removeCreateLot();
            }
        }
    }, [signer])
    useEffect(() => {
        if(InfuraProvider !== undefined){
            const ListenerContract = ethers.ContractFactory.fromSolidity(AuctionContractSol)
                .attach(process.env.REACT_APP_AUCTION_ADDRESS)
                .connect(InfuraProvider)
            const LotCreated = (value, event) => {
                eventBus.emit("TransactionHistory:addEvent", {
                    token: `Lot created`,
                    txHash: event.transactionHash
                })
                eventBus.emit("Auction:LotCreated", value)
            }
            ListenerContract.on("LotCreated", LotCreated)
            return () => {
                ListenerContract.off("LotCreated", LotCreated)
            }
        }

    }, [InfuraProvider])

    useEffect(()=>{
        if(InfuraProvider !== undefined && Signer.isSigner(signer)){
            const ListenerContract = ethers.ContractFactory.fromSolidity(AuctionContractSol)
                .attach(process.env.REACT_APP_AUCTION_ADDRESS)
                .connect(InfuraProvider)
            signer.getAddress().then(address => {
                ListenerContract.getLots(address).then((value) => {
                    eventBus.emit("Auction:UpdateAll", value)
                })
            })
        }
    }, [InfuraProvider, signer])
    return AuctionContract
}

export default useAuctionContract