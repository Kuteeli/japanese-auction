import {useEffect, useState} from "react";
import {ethers} from "ethers";



const useBlockNumber = (provider) => {
    const [blockNumber, setBlockNumber] = useState(null)

    useEffect(() => {
        if(provider === undefined) provider = new ethers.providers.Web3Provider(window.ethereum, "any");
        const onBlockUpdated = (blockNumber) => {
            setBlockNumber(blockNumber)
        }
        provider.on("block", onBlockUpdated)
        return () => {
            provider.off("block", onBlockUpdated)
        }
    }, [provider])

    return blockNumber
}

export default useBlockNumber