import {useEffect, useState} from "react";
import {ethers, Signer} from "ethers";
import LotContractSol from "../artifacts/contracts/LotRoom.sol/LotRoom.json";

const useLotContract = (signer, address)=> {
    const [LotContract, setLotContract] = useState(undefined);
    useEffect(() =>{
        if(Signer.isSigner(signer)){
            const Lot = ethers.ContractFactory.fromSolidity(LotContractSol)
                .attach(address)
                .connect(signer);
            setLotContract(Lot);
        }
    }, [signer, address])
    return [LotContract]
}

export default useLotContract