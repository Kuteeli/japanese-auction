import React, {useContext, useEffect } from 'react';
import LoginButton from "../components/buttons/LoginButton";
import {ethers} from "ethers";
import EventBus from "../services/EventBus";
import {useNavigate} from "react-router-dom";
import SignerContext from "../context/Signer";
import MetamaskInvalid from "../components/pageElements/MetamaskInvalid";

const Auth = () => {
    let navigate = useNavigate();
    const signer = useContext(SignerContext)
    useEffect(() => {
        const unsubscribeRouterPush = EventBus.on('routerPush', (page) => { navigate(page) })
        return () => {
            unsubscribeRouterPush()
        }
    }, [navigate])
    useEffect(() => {
        if (window.ethereum) {
            const updateSigner = () => {
                const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
                const signer = provider.getSigner();
                signer.getAddress().then(() => {
                    provider.getNetwork().then((res) => {
                        EventBus.emit("changeSigner", signer)
                        if (res.chainId == process.env.REACT_APP_NETWORK_VERSION) {
                            EventBus.emit("routerPush", "main")
                        } else {
                            EventBus.emit("routerPush", "error")
                        }
                    })
                }).catch((err) => {
                    EventBus.emit("changeSigner", null)
                })
            }
            updateSigner()
            window.ethereum.on('accountsChanged', function (accounts) {
                updateSigner();
            })
        }
    },[window.ethereum])
    return <div className={"auth_wrapper"}>
        {window.ethereum === undefined  ? <MetamaskInvalid/> : signer === null ? <LoginButton/> : <></> }
        <style jsx>{`
        .auth_wrapper {
          display: flex;
          justify-content: center;
          align-items: center;
          padding-top: 20vh;
        }
      `}</style>
    </div>;
};

export default Auth;