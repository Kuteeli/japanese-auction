import React, {useEffect} from 'react';
import {useNavigate} from "react-router-dom";
import {ethers} from "ethers";

const Error = () => {
    let navigate = useNavigate();

    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    provider.getNetwork().then((res) => {
        if (res.chainId == process.env.REACT_APP_NETWORK_VERSION){
            navigate("/main", {replace: true})
        }
    })

    useEffect(() => {
        window.ethereum.on('chainChanged', function (chainId) {
            if (parseInt(chainId,16) == process.env.REACT_APP_NETWORK_VERSION){
                navigate("/main", {replace: true})
            }
        })
    }, [])

    return <>
        <div id='oopss'>
            <div id='error-text'>
                <img src="https://cdn.rawgit.com/ahmedhosna95/upload/1731955f/sad404.svg" alt="404"/>
                <span>Please connect to the goerli network</span>
            </div>
        </div>
        <style jsx>{`
            #oopss {
              background: white;
              position: fixed;
              left: 0px;
              top: 0;
              width: 100%;
              height: 100%;
              z-index: 9999;
            }
            #oopss #error-text {
              font-size: 25px;
              display: flex;
              flex-direction: column;
              align-items: center;
              font-family: 'Shabnam', Tahoma, sans-serif;
              color: #000;
              direction: rtl;
            }
            #oopss #error-text img {
              margin: 85px auto 20px;
              height: 342px;
            }
            #oopss #error-text span {
              position: relative;
              font-size: 3.3em;
              font-weight: 900;
              margin-bottom: 50px;
            }
        `}
        </style>
    </>
};

export default Error;