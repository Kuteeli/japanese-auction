import React, {useEffect} from 'react';
import Header from "../components/pageElements/Header";
import RightAside from "../components/pageElements/RightAside";
import LeftAside from "../components/pageElements/LeftAside";
import {useNavigate} from "react-router-dom";

const Main = () => {
    let navigate = useNavigate();
    useEffect(() => {
        window.ethereum.on('networkChanged', function () {
            navigate("/error", {replace: true} )
        })
    }, [])
    return <>
            <Header/>
            <main>
                <LeftAside/>
                <RightAside/>
            </main>
        <style jsx>{`
          main {
            display: flex;
            align-items: center;
            justify-content: center;
          }
        `}</style>
    </>
};

export default Main;