class EventBus {
    bus = {}
    on(key, handler){
        if (this.bus[key] === undefined){
            this.bus[key] = []
        }
        this.bus[key]?.push(handler)
        return () => {
            this.off(key, handler)
        }
    }
    off(key, handler){
        const index = this.bus[key]?.indexOf(handler) ?? -1
        this.bus[key]?.splice(index >>> 0, 1)
    }
    emit(key, payload){
        this.bus[key]?.forEach((fn) => {
            try {
                fn(payload)
            } catch (e) {
                this.onError(e)
            }
        })
    }
    once(key, handler){
        const handleOnce = (payload) => {
            handler(payload)
            this.off(key, handleOnce)
        }

        this.on(key, handleOnce)
    }

    onError(error){
        return
    }
}

export default new EventBus()