module.exports = {
    networks: {
        development: {
            host: "localhost",
            port: 7545,
            network_id: "1234"
        }
    }
};